#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "medicion.h"

extern int errno;

float Temp_fahrenheit_Celsius(float temperatura);
Medicion_t lista_t[1800];

void escribirarchivo(Medicion_t *);

int main(){
	leerarchivo();
	escribirarchivo(lista_t);
	return 0;
}




void leerarchivo()
{
	FILE *fp;
	int errnum;
	char filename[MAXLINE];
	Medicion_t m;
	int i=0;
	fp = fopen("mediciones2019.csv","rb");
	if(fp == NULL){
		errnum=errno;
		fprintf(stderr,"Error al tratar de abrir el archivo es: %s \n", strerror(errnum));
	}else{
	fscanf(fp,"%*[^\n]\n");
	while(feof(fp)==0){
		
		fscanf(fp, "%s\t%s\t%f\t%f\t%d\t%f\t%f", m.date, m.time, &m.temperature, &m.humidity, &m.pressure, &m.altitude, &m.battery);
		
		strcpy(lista_t[i].date,m.date);
		strcpy(lista_t[i].time,m.time);
		lista_t[i].temperature=Temp_fahrenheit_Celsius(m.temperature);
		lista_t[i].humidity=m.humidity;
		lista_t[i].pressure=m.pressure;
		lista_t[i].altitude=m.altitude;
		lista_t[i].battery=m.battery;
		
		i=i+1;
		
		}
}
		fclose(fp);
		printf("\n se ha leido el archivo con exito\n");
	
}






//Convertir la temperatura de Fahrenheit a Celsius
		float Temp_fahrenheit_Celsius(float temperatura){
		float temp_Celsius;
		temp_Celsius=(((temperatura-32)*5)/9);
		return temp_Celsius;
	}


void escribirarchivo(Medicion_t *lista){
	int errnum;
	FILE * flujo = fopen("mediconesencelisus.csv", "w");
	if(flujo == NULL){
		errnum=errno;
		fprintf(stderr,"error en la creacion del archivo: %s \n", strerror(errnum));
	
	}else{
	int i;
	fprintf(flujo,"%s\t%s\t%s\t%s\t%s\t%s\t%s","Date","Time","Temperature","Humidity","Pressure","Altitude","Battery");
	fputc('\n',flujo);
	for(i=0; i<1800; i++){
	fprintf(flujo, "%s\t%s\t%.2f\t%.2f\t%d\t%.2f\t%.2f", lista_t[i].date, lista_t[i].time, lista_t[i].temperature, lista_t[i].humidity, 			lista_t[i].pressure, lista_t[i].altitude, lista_t[i].battery);
	
	fputc('\n', flujo);

	}
	fflush(flujo);
	fclose(flujo);
	}
	printf("\n El archivo fue creado con exito\n");

}	



